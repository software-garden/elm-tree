# Timeline for the workshop


## Day 1

### 00:00

  - 👋 👋🏾 Introduce ourselves

  - Ask to write names on badges

  - 🙋 Who read the material?

  - Organizational stuff:

    5 minute break after 1h

    20 minutes after 2h

  - 💬 Any questions?


### 00:05

  - Introduce the problem (Ellie)

  - Are there any questions?

### 00:10

  - 💻 Introduce SVG

    Installation

    Import

    Make a dot with radius only

    🙋‍ Is everyone having it working ‍

### 00:15

  - 💬 Where is the dot?

  - 💻 SVG Element - let's make it pink

  - 🙋‍ Do you have it? Is it more or less clear?

### 00:20

  - 💻 Fill the viewport

    Install Elm UI

    Import

    `svg |> html |> layout`

  - 🙋 Do you have it working?

    It's a technicality, so let's not focus too much on that right now.

### 00:30

  - Break (5 minutes)

### 00:40

  - Cartesian coordinates system

    🙋 Who knows what it is?

    👨‍🏫 Explain on the whiteboard ( ⇄ 📏 2d ☩ )

  - Change the code to move the dot ( `cx` `cy` )

    🕞 Take 3 minutes to play with it

  - 💬 Questions?


### 00:50

  - Screen size 💥

  - 👨‍🏫 ‍The infinite canvas and the viewbox into it

    We know the position of a point and we can set the viewbox any way we like. How can we make it in the middle?

  - 💻 Code

  - 💬 Questions?

### 01:10

  - Color

    💬 What else is there about our dot?

    💻 `fill "skyblue"`

    🙋‍ Who is done?

    💬 Questions

### 01:20

  - Break 20 minutes

### 01:40

  - 💬 How did you like it?

    Is there anything we could do better?

    New questions, ideas?

    Stashed questions

    See you tomorrow 👋🏾 👋
