---
presentation:
  enableSpeakerNotes: true
  theme: solarized.css
---

Steps to reproduce the tree:

Make a dot

  Centered (Elm UI, viewBox, cartesian coordinates)

Make a line

  Play with transformations (union types)

Gradients

Multiple lines

  Rosettes (different kinds)

Groups and transformations

  Spiral (recursion)

Tree


<!-- slide -->

## Before the course begins

<!-- slide -->

- Setup the development environment

  - Elm

  - Node.js

    why?

  - Test REPL (2 + 2)

  - Atom

<!-- slide -->

## Day 1

Let's make a dot!

<!-- slide -->

- Setup the project

  Do we want source control?

  - Create directory

  - `elm init`

  - Open a file in the editor

  - Type hello world program

  - Elm Reactor

  - Web browser

<!-- slide -->

### Introduce a problem

We want to have a dot on the screen

<!-- slide -->

Show complete code

<!-- slide -->

  - Explain:

    The dot has a position (x + y coordinates) and size (radius)

    Coordinate planes: we can use CartesianCoordinates

  - Introduce SVG

<!-- slide -->

- Give  a color to the dot

  - Explain SVG attributes (svg elements take a list of attributes)

  - Everyone can pick a color

<!-- slide -->

- Multiple dots

  - Introduce a problem: We want to have two dots on the screen

  - Explain a list, and show there is already a list there (with one item)

<!-- slide -->

Do you see any other lists?

  - Element takes a list of children

  - Mention, how can we have a list with one or zero elements? Shopping list with one one item. Empty spoon drawer (list of spoons)

  - Give your new dot a color and different coordinates

  - Show the code for 2 dots

  - Exercise: make 3 more dots (5 total)

<!-- slide -->

## Day 2

Place the dots in a circle

<!-- slide -->


Introduce a problem: We want to place the dots in a circle: show an example in the slides

How we will get there: we will change the cartesian coordinates of the dots, nothing else! But figuring out the right coordinates is a bit tricky

How can we figure out the correct cartesian coordinates?

<!-- slide -->

Story about the flowerpot and the table: two ways to measure relative position, one is distance from a x and y axis (cartesian coordinates), the other is angle and distance relative to origin (polar coordinates)

<!-- slide -->

What does this have to do with a circle? Do you know the expression 'I did a 180'. Where would you be looking if you did two 180s (a "360"). We see that circles and angles are closely related!

<!-- slide -->

Exercise with compass and 5 objects, place objects evenly along compass. How many degrees apart are they? (observe if we multiply the result by 5, we're back to 360)

<!-- slide -->

We have one part of it (angle), we now need the distance. Notice they are all the same distance from the origin (the center dot) of the compass. Definition of circle: points that are an equal distance from a center. Actually, the distance doesn't matter, so long as they all have the same distance. You can have a big circle or a small circle, they're both circles

<!-- slide -->

Ok great, we're done! Now, does anyone know how to give an angle and distance to svg? Oh... no? We don't either... you can't. You can only give x and y values relative to the origin

<!-- slide -->

So we already know that angle and length are just another way of describing x and y. But we need some way of translating between the two


<!-- slide -->

Using a visual demonstration, if you draw a line from your object to the x axis, you have a triangle. Same if you draw a line to the y axis. You can figure out the point on the axis using sin and cos functions and multiplying the result by the length.

<!-- slide -->

Let's use a chart to figure out the sin and cos of our angles

*It's important to get a chart, otherwise we have to use calculators which work with radians*

<!-- slide -->

Now we are done... we can plug in our x and y values, and presto, our dots are arranged in a circle. But we don't see most of them...

<!-- slide -->

Our origin is in the top left corner. This means any dots with negative x or y values are off the screen.

We can shift the dots so they are on the screen, or shift the screen so that it covers the dots. We will be shifting the screen

<!-- slide -->

Sample viewbox program to demonstrate

<!-- slide -->

Create a viewbox with correct perimeters (must be more than the radius of the circle plus the radius of a dot in each direction, and width and height are circumference)

```elm
svg [ viewbox "-100 -100 200 200" ] []
```

<!-- slide -->

## Day 3

Let the computer do the math

<!-- slide -->

Introduce the problem: Can we avoid all these repetitive and manual steps?

<!-- slide -->

What is a program made of? (in the REPL):

<!-- slide -->

Values and names

```
5
10.4
"Hello World"
[1, 2, 3, 4, 5]
```

* Numbers like `5`, `10.4`
* strings like `"Hello world"`
* and lists containing numbers or strings

are all values

<!-- slide -->

They are self-referential. They simply state what they are. There are other kinds of values that we will see later.

<!-- slide -->

You can give (or assign) a name to a value, as shown.

```
family = 5
price = 10.4
morningGreeting = "Hello World"
fingers = [1, 2, 3, 4, 5]
```

Here, `family` is a name and `5` is the value assigned to `family`.

<!-- slide -->

You can get the value back by calling its name.

```
---- Elm 0.19.0 ----------------------------------------------------------------
Read <https://elm-lang.org/0.19.0/repl> to learn more: exit, help, imports, etc.
--------------------------------------------------------------------------------
> family = 5
5 : number
> family
5 : number
```

<!-- slide -->

Operators (`+`, `-`, `++`, `::`)

```
> 2 + 5
7 : number
```

```
> 4 - 6
-2 : number
```

```
> "Hello" ++ " world!"
"Hello world!" : String
```

```
> 1 :: [2, 3, 4, 5]
[1,2,3,4,5] : List number
```

<!-- slide -->

Operators take two values and return a new value. We know that names refer to values, so we can use them in place of values:

```
> family = 5
5 : number
> family + 2
7 : number
```

<!-- slide -->

You can also give a name to the value returned by an operator:

```
> family = 5
5 : number
> familyAndPets = family + 2
7 : number
> familyAndPets
7 : number
```

<!-- slide -->

Note that different values have different types.

```
5 : number
10.4 : Float
"Hello World" : String
[1,2,3,4,5] : List number
```

<!-- slide -->

Different operators work on different types. Adding a number and a string doesn't make sense. So if you try, Elm will complain (and give a helpful hint):

```
> "Hello " + 5
-- TYPE MISMATCH ----------------------------------------------------------- elm

I cannot do addition with String values like this one:

5|   "Hello " + 5
     ^^^^^^^^
The (+) operator only works with Int and Float values.

Hint: Switch to the (++) operator to append strings!
```

(Int and Float are both types representing numbers)

<!-- slide -->

Functions

```elm
fun something = something ++ " is fun."
```

<!-- slide -->

Explain: a function is a thing that takes some values (called arguments) and return one value. So it's similar to operators. In fact operators are functions!

```elm
(-) 5 10
5 - 10
```

<!-- slide -->

You can think of a function as a machine. You put something in the machine, and it produces something in return. For example think about a machine that produces rubber ducks. You put a bucket of white plastic pellets and a bucket of red paint, and you get a bunch of red rubber ducks!

What you get will depends on what you put. The color of the ducks depends on the paint you put. Quantity of ducks depends on how much plastic you put in.

<!-- slide -->

```elm
makeMeSomeDucks color plastic =
    String.fromInt plastic ++ " " ++ color ++ " rubber ducks"
```

Once you have a function, you can call it like this:

```elm
> makeMeSomeDucks "blue" 12
"12 blue rubber ducks" : String
```

<!-- slide -->

Note: functions help organize code into nice reusable chunks.

<!-- slide -->

How do you get functions? There are three ways.

<!-- slide -->

Some functions are always there for you

`(+)`, `(-)`

<!-- slide -->

2.  Some functions you can import using code like this:

```elm
import Svg
```

and then

```
Svg.circle [ cx "10", cy "10", r "20" ] [ ]
```

To call a function that was imported, you have to prefix it with the name of the module (in this example `Svg`).

<!-- slide -->

Finally, you will write some functions, just like we saw in the `fun` and `makeMeSomeDucks` examples.

<!-- slide -->

Finally there is one special thing: the first line of the program is a module declaration. For now it's enough for us to know, that it has to be there and it has to match the name of the file.

<!-- slide -->

Exercise: In our Main.elm, try to identify some values, names and function calls:

Hint: `"darkred"` is a value, `main` is a name, `width` is a function.

Hint: There is nothing else there now, but we will soon introduce our own functions.

<!-- slide -->

#### Where can we use some functions?

As we said before, functions are good when we have some repetitive operation that can be parametrized (like rubber ducks production).

Obviously calculating `x` and `y` coordinates is repetitive and can be parametrized (parameters are `radius` and `angle`).

<!-- slide -->

#### Apply to our trigonometry calculations

Compose and copy and paste the trigonometry functions to calculate cx and cy:

```elm
cx (String.fromFloat (cos (degrees 72) * 100))
cy (String.fromFloat (sin (degrees 72) * 100))
```

<!-- slide -->

#### Eliminate the repetition:

Assign a value of `100` to a name `radius`:

```
radius = 100
```

and plug it to our functions:

```elm
cx (String.fromFloat (cos (degrees 72) * radius))
cy (String.fromFloat (sin (degrees 72) * radius))
```

<!-- slide -->

As you see the names are good for repetitive things too.

Define and reuse

- `x : angle -> cx`

  ```elm
  x angle =
      String.fromFloat (cos (degrees angle) * radius)
  ```

- `y : angle -> cy`

  ```elm
  y angle =
      String.fromFloat (sin (degrees angle) * radius)
  ```

<!-- slide -->

Then plug it into the code making circles:

```elm
circle
  [ cx (x 72)
  , cy (y 72)
  , r "10"
  , fill "darkred"
  ]
```

<!-- slide -->

define and reuse `dot : angle -> color -> Svg`

```elm
dot angle color =
    circle
        [ cx (x angle)
        , cy (y angle)
        , r "10"
        , fill color
        ]
```


<!-- slide -->

and plug it into our SVG picture:

```
svg [ viewbox "-100 -100 200 200" ]
  [ dot 0 "darkred"
  , dot 72 "fuchsia"
  , dot 144 "saddlebrown"
  , dot 216 "deepskyblue"
  , dot 288 "gold"
  ]
```

For more cool colors check out [Color keywords page at Mozilla Developer's Network](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#Color_keywords)

<!-- slide -->

- Make it more general

  - Show List and list operations (map and indexedMap)

    There are a number of functions that operate on lists, for example `List.length` and `List.map`.

<!-- slide -->

Examples of a map: a shopping list, after you find each item and place it in your basket, you are 'mapping' from a list of needed items to a list of items in your basket. Now you have two lists, and they are related. One is your original shopping list, the other the list of items in the basket.

<!-- slide -->

Demonstrate:

```elm
> things = ["Programming", "Swimming", "Dancing", "Saddle brown"]    
["Programming","Swimming","Dancing","Saddle brown"]
    : List String


> List.length things
4 : Int

> List.map fun things
["Programming is fun!","Swimming is fun!","Dancing is fun!","Saddle brown is fun!"]
    : List String

> things
["Programming","Swimming","Dancing","Saddle brown"]
    : List String
```

<!-- slide -->

Notice our original `things` list is unchanged. This is different from our rubber duck machine. The rubber duck turns the plastic and paint into rubber ducks. A function on the other hand 'creates' the value it gives you. You don't loose the original value given to it.

<!-- slide -->

Make a `palette : List color`

```elm
palette =
  [ "darkred"
  , "fuchsia"
  , "saddlebrown"
  , "deepskyblue"
  , "gold"
  ]
```

<!-- slide -->

Use `List.indexedMap dot palette` to generate the dots

<!-- slide -->

Another function that operates on lists is `List.indexedMap`. Let's see it at work:

```elm
dot index color =
    circle
        [ cx (x ((360 / (List.length palette)) * index))
        , cy (y ((360 / (List.length palette)) * index))
        , r "10"
        , fill color
        ]

List.indexedMap dot palette
```

<!-- slide -->

We can introduce a `let` block to make our code more readable and avoid repetition.

```elm
dot index color =
    let
        angle =
            (360 / count) * index

        count =
            List.length palette
    in
    circle
        [ cx (x angle)
        , cy (y angle)
        , r "10"
        , fill color
        ]
```

<!-- slide -->

Take a look at the [documentation for `List.indexedMap`](https://package.elm-lang.org/packages/elm/core/latest/List#indexedMap).

<!-- slide -->

## Day 4

<!-- slide -->

Introduce SVG groups

We'll need one group for the dots and one group for the lines

```elm
svg [ viewBox "-100 -100 200 200" ]
    [ Svg.g [] (List.indexedMap dot pallete)
    , Svg.g [] [ Svg.line [ x1 "123", y1 "112", x2 "41", y2 "11", stroke "black" ] [] ]
    ]
```

<!-- slide -->

Exercise: add a few lines with different colors

Now we'll learn how to color a line with a gradient (going from one color to another)

<!-- slide -->

```
svg [ width "600", height "600", viewBox "-300 -300 600 600" ]
    [ Svg.g [] (List.indexedMap dot pallete)
    , Svg.defs []
        [ Svg.linearGradient [ Svg.Attributes.id "MyGradient", x1 "0", y1 "0", x2 "1", y2 "0" ]
            [ Svg.stop [ Svg.Attributes.offset "0", Svg.Attributes.stopColor "saddlebrown" ] []
            , Svg.stop [ Svg.Attributes.offset "1", Svg.Attributes.stopColor "pink" ] []
            ]
        ]
    , Svg.g []
        [ Svg.line [ x1 "0", y1 "0", x2 "100", y2 "100", stroke "url(#MyGradient)" ] []
        , Svg.line [ x1 "0", y1 "0", x2 "-100", y2 "-100", stroke "url(#MyGradient)" ] []
        ]
    ]
```

<!-- slide -->

We see a problem here. The gradient always goes from saddlebrown to pink from top left to bottom right. If we want to use the same gradient to connect different points, we'll need to be creative.

<!-- slide -->

```
main =
    svg [ width "600", height "600", viewBox "-300 -300 600 600" ]
        [ Svg.g [] (List.indexedMap dot pallete)
        , Svg.defs []
            [ Svg.linearGradient [ Svg.Attributes.id "MyGradient", x1 "0", y1 "0", x2 "1", y2 "0", gradientUnits "userSpaceOnUse" ]
                [ Svg.stop [ Svg.Attributes.offset "0", Svg.Attributes.stopColor "saddlebrown" ] []
                , Svg.stop [ Svg.Attributes.offset "1", Svg.Attributes.stopColor "pink" ] []
                ]
            ]
        , Svg.g []
            [ Svg.line [ x1 "0", y1 "0", x2 "1", y2 "0", transform "rotate(45),scale(100,1)", stroke "url(#MyGradient)" ] []
            , Svg.line [ x1 "0", y1 "0", x2 "1", y2 "0", transform "rotate(180),scale(100,1)", stroke "url(#MyGradient)" ] []
            , Svg.line [ x1 "0", y1 "0", x2 "1", y2 "0", transform "rotate(270),scale(100,1)", stroke "url(#MyGradient)" ] []
            ]
        ]
```

TODO: We need to fix the viewbox!

<!-- slide -->


Here's the trick: we have the lines initially match the gradient. We're using the same x and y values for all the lines and the gradient. Then we transform the lines to position them where we'd like them. We're actually using our old friend polar coordinates here.

If you're interested in what `gradientUnits "userSpaceOnUse"` does, come see me after the lesson.

<!-- slide -->

```elm
gradient : Int -> String -> Svg msg
gradient index color =
    Svg.linearGradient
        [ id ("Gradient-" ++ String.fromInt index)
        , x1 "0"
        , y1 "0"
        , x2 "1"
        , y2 "0"
        , gradientUnits "userSpaceOnUse"
        ]
        [ Svg.stop
            [ Svg.Attributes.offset "0"
            , Svg.Attributes.stopColor "saddlebrown"
            ]
            []
        , Svg.stop
            [ Svg.Attributes.offset "1"
            , Svg.Attributes.stopColor color
            ]
            []
        ]
```

<!-- slide -->

```
line : Int -> String -> Svg msg
line index color =
    let
        count =
            List.length pallete

        angle =
            (360 / toFloat count) * toFloat index

        transformation =
            "rotate("
                ++ String.fromFloat angle
                ++ "), scale("
                ++ String.fromInt radius
                ++ ", 1)"

        url =
            "url(#Gradient-" ++ String.fromInt index ++ ")"
    in
    Svg.line
        [ x1 "0"
        , y1 "0"
        , x2 "1"
        , y2 "0"
        , transform transformation
        , stroke url
        ]
        []
```

<!-- slide -->

```
main =
    svg [ width "600", height "600", viewBox "-300 -300 600 600" ]
        [ Svg.g [] (List.indexedMap dot pallete)
        , Svg.defs [] (List.indexedMap gradient pallete)
        , Svg.g [] (List.indexedMap line pallete)
        ]
```

<!-- slide -->

    This may look like a lot, but our `gradient` and `line` functions are very similar to our `dot` function from earlier. In fact we see an essential principle of good programming design here. We spent a lot of time banging our heads over gradients. Now that we've done that work, we can hide the implementation in our gradient and line functions. Now, if we want to draw a new dot, line, and gradient going from the center to the dot, we only need to add a new item to our palette. Our functions do the rest for us. Our functions conceal our complexity. We can forget about the implementation, so long as we know how to operate them. They're like black boxes, we know what goes in and what comes out, but we don't have to know how they work on the inside.

<!-- slide -->

## Day 5

Let's make a beautiful tree!

<!-- slide -->

Our project is looking pretty good at this point. We've created some interesting graphical elements, and learned some interesting ways to place them programatically. However, it doesn't take user input like many of the programs we use. We'll be doing some pretty interesting things with user input later in this workshop, but for now we'll start with a deceptively simple step: setting the background color.

I say this step is deceptively simple, because we'll need to make some big changes to our program and introduce a number of new concepts.

<!-- slide -->

Show complete code.

<!-- slide -->

There's a lot going on here, and it's not obvious how it all works.

Our `main` value has changed.

```
main : Program () State Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }
```

<!-- slide -->

Previously, the value of `main` was an SVG element. Now it contains a call to `Browser.sandbox` function.

<!-- slide -->

A sandbox is a basic interactive program. For a program to be interactive, it need more than just a SVG element.

<!-- slide -->

```
type alias State =
    Color


type alias Color =
    String
```

Here we create something called a type alias. We've already discussed types. A type alias is basically a way to give an alternative name for a type. We give the alias `Color` to `String` and the alias `State` to `Color`. So in the end all three names point to the same type!

<!-- slide -->

Why are we doing this? All of these types are strings, and Elm will treat them all as strings. But it will make our code more readable to use these aliases. We are writing an application that allows the user to change the background color. That should explain why our `State` is a `Color`.

<!-- slide -->

Our `Browser.sandbox` takes something with the name `init`. This is our initial state. Let's take a look at the value of `init`.

```
init : State
init =
    "white"
```

<!-- slide -->

We see that the initial state is `"white"`. We'll see how our application uses this value in a moment.

<!-- slide -->

Looking back at our `sandbox`, we see it also takes a `view`. Let's take a look at our view function:

```
view : State -> Html Msg
view color =
    svg
        [ width "600"
        , height "600"
        , viewBox "-300 -300 600 600"
        , Svg.Attributes.style ("background: " ++ color)
        ]
        [ Svg.g [] (List.indexedMap dot pallete)
        , Svg.defs [] (List.indexedMap gradient pallete)
        , Svg.g [] (List.indexedMap line pallete)
        ]
```

<!-- slide -->

You'll notice that this `view` function looks very similar to the `main` variable from earlier. Before, our application was only a view. Now the view is only one piece of our interactive application.

<!-- slide -->

One important way our `view` function differs from the `main` variable from ealier is that `view` is a function. We see it takes a variable of type `State` (a string), which we have named `color`. In an Elm sandbox project the `view` always takes a variable with the same type as the `init` variable. This is the current state of the application. `view` can use the state to render the application properly, just as Atom uses its state to render text for its user.

<!-- slide -->

We see that we've added a line to the svg attributes, `Svg.Attributes.style ("background: " ++ color)`. Here we use the state of the application to set the background color of the svg element.

<!-- slide -->

All we need now is some way to update the state (the background color) of the application.

<!-- slide -->

Taking a look at our `main` function, we see that it take a third and final function, `update`. Let's look at our update function:

```
type Msg
    = SetBackground Color


update : Msg -> State -> State
update msg state =
    case msg of
        SetBackground color ->
            color

```

<!-- slide -->

We see our `update` function takes two variables, of type `Msg` and `State`. If we think of update as a machine which takes an old state and spits out a new state, this state variable corresponds to the old state.

<!-- slide -->

The `msg` variable is something new. Looking above, we see that we have defined a `Msg` variable type. This is similar to our `State` and `Color` union types, in that we have defined a new type that Elm understands. However, unlike the `type alias` constructor, the `type` constructor does not simply give a new name for an existing type. Instead, here we define a completely unique type. We see values with type Msg must have the form `SetBackground Color`. `SetBackground "white"`, `SetBackground "blue"`, `SetBackground "saddlebrown"` are all valid values of the type `Msg`.

<!-- slide -->

What is important to understand is that our `msg` variable is used by `update` to determine how it should update the state. We see that update first checks that `msg` has the form `SetBackground color`, and then returns the value of `color`. This will become our new state. Every time update is called, the sandbox will re-render our view with the new state.

<!-- slide -->

So now we see how our application changes the background color of the svg element. However, we haven't seen when it will update the background color. Well, we want to change the background color by clicking a dot. So let's take a look at our `dot` function to see if we can find a hint there.

<!-- slide -->

<dl>
<dt>Model
<dd>the structure of the state of the program
</dl>

> TODO: Consistently use `Model` and `model` instead of less standard `State` to make it easier to our participants.

<!-- slide -->

<dl>
<dt>Msg
<dd>
  What events can there be in the program. Currently have only one possible event - setting the background to a given color.
</dl>

<!-- slide -->

- `update`

  the instructions of how to change the state when a particular message comes in.

<!-- slide -->

- `view`

  the instruction of what to display on the screen. It also contains instructions of what messages to send when certain events (like clicks on elements) happen. Every time the state changes the instructions will be applied.

<!-- slide -->

- `init`

  What is the initial state right after the program is started.

<!-- slide -->

- `main`

  glues it all together.

<!-- slide -->

- How do we model the tree

<!-- slide -->

A tree is a composition of segments. A segment can be either a twig or a branch. Twig is a terminal segment of a tree. When the tree is very young it consists of a single twig. Later this twig will evolve into a branch and will grow it's own twigs, that in turn will evolve into branches. So a branch is a segment of a tree that ends with some other  segments (twigs or branches).

<!-- slide -->

Note that a branch can split to several branches that each in turn can split into several branches and so on. This way a tree can be as complex as we want. Note that real trees are like that. It's difficult to say how many generations of branches it can have from the trunk to the smallest little twigs on top of the crown. This kind of structure is called recursive. It repeats the same pattern.

<!-- slide -->

Each segment (a twig or a branch) has length, a direction (represented as an angle) and a color (so that it's beautiful despite having no leafs or flowers).

Here is how we can represent it in code:

```
type alias Segment =
    { length : Float
    , direction : Float
    , color : Color
    }
```

<!-- slide -->

If you consider that a branch can split into more branches or twigs, and then that a trunk of a tree is just a first branch or a twig if the tree is very young, then you can see that each segment separated from the parent branch can be considered a tree.

<!-- slide -->

So we can reuse the same structure to represent a tree or any of it's segments!

Look:

```
type Tree
    = Twig Segment
    | Branch Segment (List Tree)
```

<!-- slide -->

Think about it! A tree is a twig (when it's young) or a branch that splits into __a zero, one or many__ __twigs or branches__.

  - zero, one or many: a list
  - twig or branch: a tree

So a Twig is just a Segment while a branch is a Segment with a list of Trees!

<!-- slide -->

Let's encode a simple tree by hand:

```elm
tree =
  Branch { length = 10, direction = degrees -90, color = "brown" }
      [ Branch { length = 6, direction = degrees -45, color = "lightbrown" }
          [ Twig { length = 2, direction = degrees -30, color = "green" }
          , Twig { length = 2, direction = degrees -60, color = "green" }
          ]
      , Branch { length = 6, direction = degrees -135, color = "lightbrown" }
          [ Twig { length = 2, direction = degrees -120, color = "green" }
          , Twig { length = 2, direction = degrees -150, color = "green" }
          ]
      ]
```

<!-- slide -->

## Day 6

Make the tree grow!

<!-- slide -->

Make a function that given the age of the tree and rules returns a tree

<!-- slide -->

Use time subscription to make the tree grow

<!-- slide -->

```
dot : Int -> String -> Svg Msg
dot index color =
    let
        angle =
            (360 / toFloat (List.length pallete)) * toFloat index
    in
    circle
        [ cx (x angle)
        , cy (y angle)
        , r "40"
        , fill color
        , Svg.Events.onClick (SetBackground color)
        ]
        []
```

We see there is a new line here:

<!-- slide -->

```
Svg.Events.onClick (SetBackground color)
```

<!-- slide -->

`onClick` is an event. It basically tells our svg element to listen for someone to click on it. It will then handle the event by emitting the Msg `SetBackground color`. We know `color` is the color of the dot. We see then that when someone clicks on the dot, the Msg will be emitted, `update` will be called with this Msg and will update our state. Our sandbox will rerender the view with this new state. The user will see the svg element with a new background color.

<!-- slide -->

Notice that our `view` function, as well as all the functions that call functions responsible for rendering svg elements, including `gradient`, `line`, and `dot` have return values with type `Svg Msg` or `Html Msg`. This is because svg element can emit Msgs which will be handled by `update`.


<!-- slide -->

# Software Garden
## A functional programming workshop
### for non-programmers

<!-- slide -->

## Setup

<!-- slide -->

> This setup instructions are based on an assumption that you are using a Mac.
>
> If you are using Linux or BSD, then you probably know how to install stuff.
>
> If you are using anything else, then... well, good luck.
>
> The rest of the instructions should work with any platform.

<!-- slide -->

We will need:

- a text editor (I use [Atom][])
- and the [Elm programming language][]

[Atom]: https://atom.io/
[Elm programming language]: https://elm-lang.org/

<!-- slide -->

### Elm Installation

<!-- slide -->

To install the Elm programming language, go to it's website:

http://elm-lang.org/

and follow the installation instructions.

<!-- slide -->

Some of the tools we use with Elm require Node.js.

Go to the [Node.js website][Node.js] and install the current version (the green button on the right).

[Node.js]: https://nodejs.org/en/

<!-- slide -->

We will need to use the terminal a little bit.

# :fa-terminal:

Don't be scared. It's easy :)

<!-- slide data-background-image="images/mac-launchpad-terminal.png" data-background-size="cover" data-background-position="top center"-->

<p style="color: white">
  In Launchpad find a program called <code>terminal</code> and start it.
</p>

<!-- slide -->

You should see a window like this

<!-- slide -->

<img alt="Mac Terminal app window" src="../assets/mac-terminal-window.png" class="plain"/>

<!-- slide -->

Now we are going to install few things.

- Homebrew (to install other things)
- Elm programming language
- Atom editor

<!-- slide -->

### Install Homebrew

Follow instructions on the [Homebrew website](https://brew.sh/) by typing the following in the terminal (you probably want to copy and paste it):

```sh
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

<small>
Make sure that the command is exactly like the one above, including the `/` character at the beginning, the quotes and parentheses.

It will ask you to confirm several actions and ask for your password. It may take few minutes to finish, so get your coffee
</small>

:fa-coffee:

Once it's done you should see a command prompt like that:

```
~ your-name$
```


<!-- slide -->

### Install the Elm programming language

<small>Once we have Homebrew installed, we can use it to install the language.</small>

Type the following in the terminal.

```sh
brew install node elm
```

and check if it works by typing:

```
elm repl
```

<!-- slide -->

Note that the command line changes. You should see something like that

```
---- Elm 0.19.0 ----------------------------------------------------------------
Read <https://elm-lang.org/0.19.0/repl> to learn more: exit, help, imports, etc.
--------------------------------------------------------------------------------
>
```

It's the Elm REPL

<small>Read - Evaluate - Print Loop</small>

*[REPL]: Read - Evaluate - Print Loop.

<!-- slide -->

Inside the REPL type

```
2 + 2
```

And expect to see

```
---- Elm 0.19.0 ----------------------------------------------------------------
Read <https://elm-lang.org/0.19.0/repl> to learn more: exit, help, imports, etc.
--------------------------------------------------------------------------------
> 2 + 2
4 : number
>
```

Easy, huh?

<!-- slide -->

We will learn more about REPL later. For now type `:exit` to close it.

The command line should look like before again.

<!-- slide -->

### Install Atom text editor

Computer programs are represented as text, so the text editor is the most fundamental tool of a programmer. There is a lot of good text editors, but to get you started, we will use [Atom] here.

<!-- slide -->

Type following in the terminal:

```
brew cask install atom
```

And start it with:

```sh
atom
```

<!-- slide -->

One last thing we need is Elm support for Atom editor. You can install it by typing in the terminal:

```
apm install language-elm
```

<small>APM is the Atom Package Manager, but don't pay much attention to it. We won't be using it again.</small>

<!-- slide -->

**We are all set!**

:smile:

<!-- slide -->

# First program!

<!-- slide -->

As mentioned before, programs are represented as text (called *the source code*).

The source code is stored in files

:fa-file:

and files are organized in directories

:fa-folder:

<!-- slide -->

So the first step is to create a directory for our new program. Let's call it `fpart`.

In the terminal type

```sh
mkdir fpart/
```

and then

```
cd fpart/
```

<small>This creates a new directory and makes it the current one. Again, don't worry about the details.</small>

<!-- slide -->

To easily create a new program, we can type

```
elm init
```

<!-- slide -->

Then to create a file with source code, type

```
atom src/Main.elm
```

<small>This command should open a new Atom window with empty text file.</small>

<!-- slide -->

### `main.elm`

```elm
module Main exposing (main)

import Html


main =
    Html.text "Hello, Tree!"
```

<small>Type the above in the editor and save the file</small>

<!-- slide -->

To see the program running type following in the terminal

```sh
elm reactor
```

<small>This command starts the Elm reactor, which will let you run your program in the web browser. Note that it won't give you the command line back - it will run as long as you don't stop it.</small>

<!-- slide -->

# Voila!

<small>Open following address in the web browser</small>

http://localhost:8000/src/Main.elm

<small>Note that the same address was printed by Elm reactor</small>

<!-- slide -->

# Let's make a dot!
# :fa-circle:

<!-- slide -->

We are going to use a technology called SVG

<small>Scalable Vector Graphics</small>


*[SVG]: Scalable Vector Graphics

<!-- slide -->

Let's install an Elm package to help us work with SVG.

Stop the Reactor running in terminal by pressing

`CTRL-C`

and type the following

```
elm install elm/svg
```

Then start the reactor again

```
elm reactor
```

<small>you can press up arrow :fa-arrow-up: on the keyboard to get to previous commands</small>


<!-- slide -->

Move sliders to change the `x` and `y` coordinates of the dot. It's like moving a thing on a table by telling how far left, right, up or down should it go.

`<CartesianCoordinates program>`

<!-- slide -->

Move sliders to change the `angle` and `length` properties of the line connecting the dot and origin. The x and y coordinates will be calculated like this:

```
x = cos(angle) * length
y = sin(angle) * length
```


`<PolarCoordinates program>`
