module Examples.Transformations exposing
    ( Model
    , Msg
    , init
    , main
    , ui
    , update
    )

import Array exposing (Array)
import Browser
import Browser.Events
import CartesianPlane
import Dict exposing (Dict)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Input as Input
import Geometry.Svg
import Html exposing (Html)
import Json.Decode exposing (Decoder)
import LineSegment2d
import List.Extra as List
import Point2d
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Transformations exposing (Transformation(..))


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }


type alias Model =
    Array Transformation


type Msg
    = AddTransformation Transformation
    | DeleteTransformation Int
    | SetTransformation Int Transformation


init : Model
init =
    Array.fromList
        [ Translate 0 0
        , Rotate 0
        , Scale 1 1
        ]


view : Model -> Html Msg
view model =
    model
        |> ui
        |> Element.el
            [ Element.width (Element.maximum 600 Element.fill)
            , Element.centerX
            ]
        |> Element.layout
            [ Element.height Element.fill
            , Element.width Element.fill
            ]


ui : Model -> Element Msg
ui model =
    let
        transformations =
            Array.toList model

        wrapper element =
            Element.column
                [ Element.width Element.fill
                , Element.spacing 20
                ]
                [ Element.el
                    [ Element.width Element.fill
                    ]
                    (Element.html element)
                , Element.el
                    [ Background.color (Element.rgb 1 0.8 0.8)
                    , Element.padding 10
                    , Element.width Element.fill
                    ]
                    (transformationsUI transformations)
                ]

        shape =
            g
                [ transformations
                    |> List.map Transformations.toString
                    |> String.join " "
                    |> transform
                ]
                [ line
                    [ x1 "0"
                    , x2 "100"
                    , y1 "0"
                    , y2 "0"
                    , stroke "red"
                    , strokeWidth "1"
                    ]
                    []
                , circle [ cx "0", cy "0", r "2", fill "red" ] []
                , grid
                    [ stroke "pink"
                    , fill "pink"
                    , strokeWidth "0.3"
                    ]
                    10
                    30

                -- , rect [ x "", y "-2", width "1", height "4" ] []
                ]
    in
    shape
        |> List.singleton
        |> CartesianPlane.graph
            { minX = -150
            , minY = -150
            , maxX = 150
            , maxY = 150
            }
        |> wrapper


transformationsUI : List Transformation -> Element Msg
transformationsUI transformations =
    let
        addButtons =
            [ Element.text "Add transformation: "
            , Input.button []
                { onPress = Just (AddTransformation (Translate 0 0))
                , label = Element.text "Translate"
                }
            , Input.button []
                { onPress = Just (AddTransformation (Scale 1 1))
                , label = Element.text "Scale"
                }
            , Input.button []
                { onPress = Just (AddTransformation (Rotate 0))
                , label = Element.text "Rotate"
                }
            ]

        currentTrasformations =
            transformations
                |> List.indexedMap transformationUI
    in
    Element.column
        [ Element.width Element.fill
        , Element.spacing 10
        ]
        [ Element.row
            [ Element.width Element.fill
            , Element.spacing 10
            ]
            addButtons
        , Element.column
            [ Element.width Element.fill
            , Element.spacing 10
            ]
            currentTrasformations
        ]


transformationUI : Int -> Transformation -> Element Msg
transformationUI index transformation =
    let
        sliderBackground =
            Element.el
                [ Element.width Element.fill
                , Element.height (Element.px 2)
                , Element.centerY
                , Background.color <| Element.rgb 0.7 0.7 0.7
                , Border.rounded 2
                ]
                Element.none

        controls =
            case transformation of
                Identity ->
                    [ Element.text "Identity" ]

                Scale horizontal vertical ->
                    [ Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \x ->
                                SetTransformation index (Scale x vertical)
                        , label = Input.labelLeft [] (Element.text "horizontal")
                        , min = 0
                        , max = 10
                        , value = horizontal
                        , thumb = Input.defaultThumb
                        , step = Just 0.1
                        }
                    , Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \y ->
                                SetTransformation index (Scale horizontal y)
                        , label = Input.labelLeft [] (Element.text "vertical")
                        , min = 0
                        , max = 10
                        , value = vertical
                        , thumb = Input.defaultThumb
                        , step = Just 0.1
                        }
                    ]

                Translate x y ->
                    [ Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \value ->
                                SetTransformation index (Translate value y)
                        , label = Input.labelLeft [] (Element.text "x")
                        , min = -100
                        , max = 100
                        , value = x
                        , thumb = Input.defaultThumb
                        , step = Just 1
                        }
                    , Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \value ->
                                SetTransformation index (Translate x value)
                        , label = Input.labelLeft [] (Element.text "y")
                        , min = -100
                        , max = 100
                        , value = y
                        , thumb = Input.defaultThumb
                        , step = Just 1
                        }
                    ]

                Rotate angle ->
                    [ Input.slider
                        [ Element.behindContent sliderBackground
                        ]
                        { onChange =
                            \value ->
                                SetTransformation index (Rotate value)
                        , label = Input.labelLeft [] (Element.text "angle")
                        , min = -360
                        , max = 360
                        , value = angle
                        , thumb = Input.defaultThumb
                        , step = Just 1
                        }
                    ]
    in
    Element.column
        [ Element.width Element.fill
        , Border.color (Element.rgb 0.9 0.9 0.9)
        , Border.width 3
        , Element.padding 5
        , Element.spacing 20
        ]
        [ Element.row [ Element.width Element.fill ]
            [ transformation
                |> Transformations.toString
                |> Element.text
                |> Element.el [ Element.width Element.fill ]
            , Input.button []
                { onPress = Just (DeleteTransformation index)
                , label = Element.text "X"
                }
            ]
        , Element.column
            [ Element.width Element.fill
            , Element.spacing 20
            ]
            controls
        ]


update : Msg -> Model -> Model
update msg model =
    case msg of
        AddTransformation transformation ->
            Array.push transformation model

        DeleteTransformation index ->
            let
                end =
                    Array.length model

                front =
                    Array.slice 0 index model

                back =
                    Array.slice (index + 1) end model
            in
            Array.append front back

        SetTransformation index transformation ->
            Array.set index transformation model


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


grid : List (Svg.Attribute msg) -> Float -> Float -> Svg msg
grid attributes unit size =
    let
        positiveValues =
            size
                / 2
                |> floor
                |> List.range 1
                |> List.map toFloat
                |> List.map ((*) unit)

        negativeValues =
            positiveValues
                |> List.map negate

        max =
            unit * size / 2

        min =
            negate max
    in
    ((positiveValues ++ negativeValues)
        |> List.map
            (\value ->
                [ ( Point2d.fromCoordinates ( value, min )
                  , Point2d.fromCoordinates ( value, max )
                  )
                , ( Point2d.fromCoordinates ( min, value )
                  , Point2d.fromCoordinates ( max, value )
                  )
                ]
            )
        |> List.concat
        |> List.map LineSegment2d.fromEndpoints
        |> List.map (Geometry.Svg.lineSegment2d attributes)
    )
        |> (::)
            (CartesianPlane.axes attributes
                { minX = -150
                , minY = -150
                , maxX = 150
                , maxY = 150
                }
            )
        |> g []
