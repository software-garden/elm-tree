module Examples.Spiral exposing (main, ui)

import Element
import Svg
import Svg.Attributes


main =
    Element.layout
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        ui


ui =
    let
        defs =
            Svg.defs []
                [ Svg.linearGradient
                    [ Svg.Attributes.id "blue-pink-gradient"
                    , Svg.Attributes.x1 "0"
                    , Svg.Attributes.y1 "0"
                    , Svg.Attributes.x2 "1"
                    , Svg.Attributes.y2 "0"
                    , Svg.Attributes.gradientUnits "userSpaceOnUse"
                    ]
                    [ Svg.stop
                        [ Svg.Attributes.stopColor "blue"
                        , Svg.Attributes.offset "0"
                        ]
                        []
                    , Svg.stop
                        [ Svg.Attributes.stopColor "pink"
                        , Svg.Attributes.offset "1"
                        ]
                        []
                    ]
                ]
    in
    Element.html
        (Svg.svg
            [ Svg.Attributes.viewBox "-100 -100 200 200"
            ]
            (defs :: spiral 50)
        )


spiral : Int -> List (Svg.Svg msg)
spiral age =
    if age > 0 then
        let
            length =
                50 / toFloat age
        in
        [ Svg.line
            [ Svg.Attributes.x2 "1"
            , Svg.Attributes.stroke "url(#blue-pink-gradient)"
            , transform [ Scale length 1 ]
            ]
            []
        , Svg.g
            [ transform
                [ Identity
                , Translate length 0
                , Rotate 15
                ]
            ]
            (spiral (age - 1))
        ]

    else
        []


type Transformation
    = Identity
    | Scale Float Float
    | Translate Float Float
    | Rotate Float


transform : List Transformation -> Svg.Attribute msg
transform transformations =
    let
        toString : Transformation -> String
        toString transformation =
            case transformation of
                Identity ->
                    ""

                Scale x y ->
                    "scale("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Translate x y ->
                    "translate("
                        ++ String.fromFloat x
                        ++ ", "
                        ++ String.fromFloat y
                        ++ ")"

                Rotate angle ->
                    "rotate("
                        ++ String.fromFloat angle
                        ++ ")"
    in
    transformations
        |> List.map toString
        |> String.join " "
        |> Svg.Attributes.transform
