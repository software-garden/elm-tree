module Examples.Protractor exposing (Config, defaults, main, ui)

import Element exposing (Element)
import Html exposing (Html)
import Protractor
import Svg exposing (Svg)
import Svg.Attributes exposing (fill, stroke, viewBox)


type alias Config =
    { radius : Float
    , strokeWidth : Float
    }


defaults : Config
defaults =
    { radius = 500
    , strokeWidth = 1
    }


main : Html msg
main =
    ui defaults
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


ui : Config -> Element msg
ui config =
    Protractor.protractor config
        |> List.singleton
        |> Svg.svg
            [ Svg.Attributes.viewBox "-500 -500 1000 1000"
            , Svg.Attributes.width "100%"
            , Svg.Attributes.height "100%"
            ]
        |> Element.html
