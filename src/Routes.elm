module Routes exposing
    ( Route(..)
    , parse
    , parser
    )

import Url exposing (Url)
import Url.Parser as Parser exposing (..)


type Route
    = Home
    | Content String
    | Motivation
    | TestRun
    | SecondWorkshop
    | NotFound


parser : Parser (Route -> b) b
parser =
    Parser.oneOf
        [ Parser.map Home Parser.top
        , Parser.map Home (Parser.s "index.html")
        , Parser.map Motivation (Parser.s "motivation.html")
        , Parser.map TestRun (Parser.s "test-run.html")
        , Parser.map SecondWorkshop (Parser.s "second-workshop.html")
        , Parser.custom "Content"
            (\path ->
                case String.split "." path of
                    [ base, "html" ] ->
                        Just (Content base)

                    _ ->
                        Nothing
            )
        ]


parse : Url -> Route
parse url =
    url
        |> Parser.parse parser
        |> Maybe.withDefault NotFound
