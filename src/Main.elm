module Main exposing (main)

{-| This program is responsible for rendering the website.

It fetches the Elm Markup file at /index.txt and renders it. There are a number of embeded programs in the markup.

-}

import Basics.Extra exposing (curry)
import Browser
import Browser.Dom as Dom exposing (Viewport)
import Browser.Events
import Browser.Navigation as Navigation
import BrowserWindow
import Dict
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Events
import Element.Extra as Element
import Element.Font as Font
import Element.Input as Input
import Element.Lazy
import Examples
import Examples.AnimatedTree
import Examples.CartesianCoordinates
import Examples.Circle
import Examples.Counter
import Examples.Gradient
import Examples.NestedTransformations
import Examples.PolarCoordinates
import Examples.Protractor
import Examples.RosetteTypedTransformations
import Examples.Shapes
import Examples.Spiral
import Examples.Transformations
import Examples.Tree
import Examples.ViewBox
import FeatherIcons exposing (icons)
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Http
import Json.Decode exposing (Decoder)
import List.Extra as List
import Mark
import Mark.Custom
import Parser
import Parser.Advanced
import Result.Extra as Result
import Routes exposing (Route)
import Spring exposing (Spring)
import Svg.Attributes
import Task
import Time
import Transformations exposing (Transformation)
import Url exposing (Url)


main : Program Flags Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = UrlRequested
        }


type alias Flags =
    { markup : Maybe String
    }


type alias Model =
    { location : Url
    , key : Navigation.Key
    , content : Content
    , examples : Examples.Model
    , scroll : Spring
    , viewport : { width : Int, height : Int }
    }


type Msg
    = NoOp
    | Animate Float
    | UrlRequested Browser.UrlRequest
    | UrlChanged Url
    | ContentFetched (Result Http.Error String)
    | ExamplesMsg Examples.Msg
    | ContainerMeasured (Result Dom.Error Dom.Viewport)
    | ViewportMeasured { width : Int, height : Int }
    | Tick Time.Posix


type Content
    = Loading
    | FetchError Http.Error
    | ParseError (List DeadEnd)
    | Loaded (Model -> View)


type alias DeadEnd =
    Parser.Advanced.DeadEnd Mark.Context Mark.Problem


type alias View =
    { title : String
    , body : Element Msg
    }


type alias Document =
    Mark.Document (Model -> View)


init : Flags -> Url -> Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( examplesModel, examplesCmd ) =
            Examples.init

        content =
            case flags.markup of
                Nothing ->
                    Loading

                Just markup ->
                    markup
                        |> Mark.parse document
                        |> Result.map Loaded
                        |> Result.extract ParseError
    in
    ( { location = url
      , key = key
      , content = content
      , examples = examplesModel
      , scroll = Spring.create { strength = 20, dampness = 4 }
      , viewport =
            { width = 800, height = 600 }
      }
    , Cmd.batch
        [ if content == Loading then
            url
                |> Routes.parse
                |> loadContent

          else
            Cmd.none
        , Dom.getViewport
            |> Task.map
                (\{ viewport } ->
                    { width = round viewport.width
                    , height = round viewport.height
                    }
                )
            |> Task.perform ViewportMeasured
        , Cmd.map ExamplesMsg examplesCmd
        ]
    )


view : Model -> Browser.Document Msg
view model =
    let
        content : View
        content =
            case model.content of
                Loading ->
                    loadingView

                FetchError error ->
                    (case error of
                        Http.BadUrl message ->
                            "Malformed URL: " ++ message

                        Http.Timeout ->
                            "Request timeout"

                        Http.NetworkError ->
                            "Network error"

                        Http.BadStatus status ->
                            "Bad response status: " ++ String.fromInt status

                        Http.BadBody message ->
                            "Malformed response body: " ++ message
                    )
                        |> Element.text
                        |> View "Error fetching content"

                ParseError deadEnds ->
                    deadEndsView deadEnds

                Loaded documentView ->
                    documentView model

        navigationBar =
            case Routes.parse model.location of
                Routes.Home ->
                    -- TODO: Implement fragment identifier navigation
                    -- homeNavigationBar
                    Element.none

                Routes.Motivation ->
                    Element.none

                Routes.TestRun ->
                    Element.none

                Routes.SecondWorkshop ->
                    Element.none

                Routes.Content _ ->
                    contentNavigationBar model

                Routes.NotFound ->
                    contentNavigationBar model

        loadingView : View
        loadingView =
            { title = "Loading Content"
            , body =
                "Loading content..."
                    |> Element.text
                    |> Element.el
                        [ Element.centerX
                        , Element.centerY
                        ]
            }

        deadEndsView : List DeadEnd -> View
        deadEndsView deadEnds =
            deadEnds
                |> List.map deadEndElement
                |> Element.column
                    [ Element.centerX
                    , Element.centerY
                    , Background.color Mark.Custom.colors.maroon
                    , Element.padding 40
                    , Element.spacing 20
                    ]
                |> View "Parsing Errors"

        deadEndElement : DeadEnd -> Element Msg
        deadEndElement { row, col, problem, contextStack } =
            let
                message =
                    case problem of
                        Mark.ExpectingIndent level ->
                            "Expecting indentation level "
                                ++ String.fromInt level

                        Mark.InlineStart ->
                            "Expecting inline start"

                        Mark.InlineEnd ->
                            "Expecting inline end"

                        Mark.BlockStart ->
                            "Expecting block start"

                        Mark.Expecting what ->
                            "Expecting " ++ what

                        Mark.ExpectingBlockName name ->
                            "Expecting a block name " ++ name

                        Mark.ExpectingInlineName name ->
                            "Expecting an inline name " ++ name

                        Mark.ExpectingFieldName name ->
                            "Expecting a field name " ++ name

                        Mark.NonMatchingFields { expecting, found } ->
                            "Fields don't match. Expecting one of [ "
                                ++ String.join ", " expecting
                                ++ " ], but got [ "
                                ++ String.join ", " found
                                ++ " ]"

                        Mark.MissingField name ->
                            "A field is missing from the record: "
                                ++ name

                        Mark.RecordError ->
                            "A record error"

                        Mark.Escape ->
                            "Escape"

                        Mark.EscapedChar ->
                            "Escaped a character"

                        Mark.Newline ->
                            "Expecting a newline"

                        Mark.Space ->
                            "Expecting a space"

                        Mark.End ->
                            "Expecting an end"

                        Mark.Integer ->
                            "Expecting an integer"

                        Mark.FloatingPoint ->
                            "ExpectingIndent a floating point number"

                        Mark.InvalidNumber ->
                            "Invalid number"

                        Mark.UnexpectedEnd ->
                            "Unexpected end"

                        Mark.CantStartTextWithSpace ->
                            "Can't start text with a space"

                        Mark.UnclosedStyles styles ->
                            let
                                styleName : Mark.Style -> String
                                styleName style =
                                    case style of
                                        Mark.Bold ->
                                            "Bold"

                                        Mark.Italic ->
                                            "Italic"

                                        Mark.Strike ->
                                            "Strike"
                            in
                            "Unclosed styles: [ "
                                ++ (styles
                                        |> List.map styleName
                                        |> String.join ", "
                                   )
                                ++ "]"

                        Mark.UnexpectedField { found, options, recordName } ->
                            "Unexpected field: "
                                ++ found
                                ++ ". Valid fields for "
                                ++ recordName
                                ++ " are: [ "
                                ++ String.join ", " options
                                ++ " ]"
            in
            Element.text
                (message
                    ++ " at "
                    ++ String.fromInt row
                    ++ ":"
                    ++ String.fromInt col
                )
    in
    { title = "Software Garden : " ++ content.title
    , body =
        [ content.body
            |> Element.layout
                [ -- Below is a hack that enables static site generation
                  Element.htmlAttribute (Html.Attributes.id "app-container")
                , Element.htmlAttribute (Html.Attributes.lang "en")
                , Font.family [ Font.typeface "Montserrat", Font.sansSerif ]
                , Element.css "text-size-adjust" "none"
                , Element.css "-webkit-text-size-adjust" "none"
                , Element.css "-moz-text-size-adjust" "none"
                , Element.css "-ms-text-size-adjust" "none"
                , Element.inFront navigationBar
                ]
        ]
    }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model
            , Cmd.none
            )

        UrlRequested (Browser.Internal url) ->
            ( model
            , url
                |> Url.toString
                |> Navigation.pushUrl model.key
            )

        UrlRequested (Browser.External url) ->
            ( model
            , Navigation.load url
            )

        UrlChanged url ->
            ( { model
                | location = url
                , content = Loading
                , scroll =
                    model.scroll
                        |> Spring.setTarget 0
                        |> Spring.jumpTo 0
              }
            , Cmd.batch
                [ url
                    |> Routes.parse
                    |> loadContent
                , Dom.setViewport 0 0
                    |> Task.perform (always NoOp)
                ]
            )

        ContentFetched (Ok markup) ->
            ( { model
                | content =
                    markup
                        |> Mark.parse document
                        |> Result.map Loaded
                        |> Result.extract ParseError
              }
            , Cmd.none
            )

        ContentFetched (Err error) ->
            ( { model | content = FetchError error }
            , Cmd.none
            )

        ExamplesMsg examplesMsg ->
            let
                ( examplesModel, examplesCmd ) =
                    Examples.update examplesMsg model.examples
            in
            ( { model | examples = examplesModel }
            , Cmd.map ExamplesMsg examplesCmd
            )

        ContainerMeasured (Ok { viewport, scene }) ->
            let
                scroll =
                    if (scene.height - viewport.height) == 0 then
                        0

                    else
                        viewport.y / (scene.height - viewport.height)
            in
            ( { model
                | scroll =
                    Spring.setTarget scroll model.scroll
              }
            , Cmd.none
            )

        ContainerMeasured (Err error) ->
            ( model
            , Cmd.none
            )

        ViewportMeasured viewport ->
            ( { model | viewport = viewport }
            , Cmd.none
            )

        Animate delta ->
            ( { model
                | scroll =
                    model.scroll
                        |> Spring.animate delta
              }
            , Cmd.none
            )

        Tick time ->
            ( model
            , Dom.getViewport
                |> Task.attempt ContainerMeasured
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ model.examples
            |> Examples.subscriptions
            |> Sub.map ExamplesMsg
        , Browser.Events.onResize
            (\width height ->
                ViewportMeasured { width = width, height = height }
            )
        , if Spring.atRest model.scroll then
            Sub.none

          else
            Browser.Events.onAnimationFrameDelta Animate
        , Time.every 250 Tick
        ]


loadContent : Route -> Cmd Msg
loadContent route =
    case route of
        Routes.Home ->
            Http.get
                { url = "/content/index.txt"
                , expect = Http.expectString ContentFetched
                }

        Routes.Motivation ->
            Http.get
                { url = "/content/motivation.txt"
                , expect = Http.expectString ContentFetched
                }

        Routes.TestRun ->
            Http.get
                { url = "/content/test-run.txt"
                , expect = Http.expectString ContentFetched
                }

        Routes.SecondWorkshop ->
            Http.get
                { url = "/content/second-workshop.txt"
                , expect = Http.expectString ContentFetched
                }

        Routes.Content base ->
            Http.get
                { url = "/content/" ++ base ++ ".txt"
                , expect = Http.expectString ContentFetched
                }

        Routes.NotFound ->
            Cmd.none


homeNavigationBar : Element Msg
homeNavigationBar =
    Element.row
        [ Element.width Element.fill
        , Element.padding 40
        , Element.spacing 80
        , Font.bold
        , Background.color (Element.rgb 1 1 1)
        ]
        [ Element.link
            [ Element.alignLeft
            ]
            { url = "/"
            , label = Element.text "Home"
            }
        , Element.link
            [ Element.alignRight
            ]
            { url = "#about-us"
            , label = Element.text "About Us"
            }
        , Element.link
            []
            { url = "#contact"
            , label = Element.text "Contact"
            }
        ]


contentNavigationBar : Model -> Element Msg
contentNavigationBar { location, scroll, viewport } =
    let
        links :
            List
                { url : String
                , label : String
                , icon : Element msg
                }
        links =
            [ { url = "/"
              , label = "Home"
              , icon =
                    FeatherIcons.home
                        |> FeatherIcons.toHtml
                            [ Html.Attributes.style "height" "1em" ]
                        |> Element.html
              }
            , { url = "/preparation.html"
              , label = "Get ready"
              , icon =
                    FeatherIcons.bookOpen
                        |> FeatherIcons.toHtml
                            [ Html.Attributes.style "height" "1em" ]
                        |> Element.html
              }
            , { url = "/day-1.html"
              , label = "Day 1"
              , icon = Element.text "1"
              }
            , { url = "/day-2.html"
              , label = "Day 2"
              , icon = Element.text "2"
              }
            , { url = "/day-3.html"
              , label = "Day 3"
              , icon = Element.text "3"
              }
            , { url = "/day-4.html"
              , label = "Day 4"
              , icon = Element.text "4"
              }
            , { url = "/day-5.html"
              , label = "Day 5"
              , icon = Element.text "5"
              }
            ]

        linkElement :
            { url : String
            , label : String
            , icon : Element Msg
            }
            -> Element Msg
        linkElement link =
            Element.link
                ([ Element.width Element.fill
                 , Font.size 14
                 , Font.light
                 , Element.paddingEach
                    { top = 20
                    , right = 5
                    , bottom = 10
                    , left = 5
                    }
                 , Font.color Mark.Custom.colors.gray
                 ]
                    ++ (if location.path == link.url then
                            currentAttributes

                        else
                            []
                       )
                )
                { url = link.url
                , label =
                    Element.el [ Element.centerX ]
                        (case ( device.class, device.orientation ) of
                            ( Element.Phone, Element.Portrait ) ->
                                Element.el
                                    [ Font.size 24
                                    , Element.paddingXY 0 20
                                    ]
                                    link.icon

                            ( Element.Tablet, Element.Portrait ) ->
                                Element.el
                                    [ Font.size 24
                                    , Element.paddingXY 0 20
                                    ]
                                    link.icon

                            _ ->
                                Element.text link.label
                        )
                }

        linksRow =
            links
                |> List.map linkElement
                |> Element.row
                    [ Element.width Element.fill
                    , Element.spaceEvenly
                    , Font.bold
                    ]

        currentAttributes =
            [ Font.medium
            , Font.color Mark.Custom.colors.blue
            , Font.size 18
            ]

        device =
            Element.classifyDevice viewport

        progressBar : Int -> Int -> Float -> Element Msg
        progressBar total done progress =
            Element.row [ Element.width Element.fill ]
                [ Element.el
                    -- Previous days
                    [ Element.width (Element.fillPortion done)
                    , Element.height (Element.px 4)
                    , Background.color Mark.Custom.colors.green
                    ]
                    Element.none
                , Element.row
                    -- Current day
                    [ Element.width (Element.fillPortion 1)
                    , Element.height (Element.px 4)
                    ]
                    [ Element.el
                        -- Done
                        [ Element.width
                            (Element.fillPortion (round (progress * 1000)))
                        , Element.height (Element.px 4)
                        , Background.color Mark.Custom.colors.green
                        , Element.onRight
                            (Element.el
                                -- A dot at the end of the progress bar
                                [ Element.width (Element.px 10)
                                , Element.height (Element.px 10)
                                , Element.moveUp 3
                                , Border.rounded 10
                                , Border.color Mark.Custom.colors.green
                                , Border.width 3
                                , Element.moveLeft 5
                                , Background.color Mark.Custom.colors.white
                                ]
                                Element.none
                            )
                        ]
                        Element.none
                    , Element.el
                        -- Ahead
                        [ Element.width
                            (Element.fillPortion (1000 - round (progress * 1000)))
                        , Element.height (Element.px 4)
                        ]
                        Element.none
                    ]
                , Element.el
                    -- Days ahead
                    [ Element.width (Element.fillPortion (total - done - 1))
                    , Element.height (Element.px 4)
                    ]
                    Element.none
                ]

        route =
            Routes.parse location

        past : Int
        past =
            links
                |> List.map .url
                |> List.map (\newPath -> { location | path = newPath })
                |> List.map Routes.parse
                |> List.elemIndex route
                |> Maybe.withDefault 0
    in
    Element.column
        [ Element.width Element.fill
        , Background.color (Element.rgb 1 1 1)
        , Border.shadow
            { offset = ( 0, 0 )
            , size = 1
            , blur = 3
            , color =
                if Spring.target scroll == 0 then
                    Element.rgba 0.8 0.8 0.8 0

                else
                    Element.rgb 0.8 0.8 0.8
            }
        , Element.htmlAttribute (Html.Attributes.id "navigation-bar")
        ]
        [ linksRow
        , scroll
            |> Spring.value
            |> progressBar
                (List.length links)
                past
        ]


document : Document
document =
    let
        page :
            { title : String
            , children : List (Examples.Model -> Element Msg)
            }
            -> Model
            -> View
        page { title, children } model =
            View title (Element.Lazy.lazy3 body children model.examples model.viewport)

        body children model viewport =
            children
                |> List.map (\child -> child model)
                |> Element.textColumn
                    (viewport
                        |> Element.classifyDevice
                        |> responsiveAttributes
                        |> (++)
                            [ Element.centerX
                            , Element.spacing 20
                            ]
                    )

        responsiveAttributes { class, orientation } =
            case ( class, orientation ) of
                ( Element.Phone, Element.Portrait ) ->
                    [ Element.width Element.fill
                    , Element.paddingXY 10 80
                    ]

                ( Element.Tablet, Element.Portrait ) ->
                    [ Element.width Element.fill
                    , Element.paddingXY 20 80
                    ]

                _ ->
                    [ Element.width
                        (Element.fill
                            |> Element.maximum 960
                            |> Element.minimum 500
                        )
                    , Element.paddingXY 0 80
                    ]

        {- The document has to start with a Title block containing a String (i.e. single line of unforamtted text). This String will be used in two ways:

           It will be turned into an Element and appended to list of children that are passed to the view (so it can be rendered in the body of the page).

           It will also serve as a title for a Browser.Document.
        -}
        structure =
            Mark.startWith
                (\title rest ->
                    let
                        titleElement model =
                            title
                                |> Element.text
                                |> List.singleton
                                |> Element.paragraph
                                    [ Element.width Element.fill
                                    , Element.paddingXY 0 32
                                    , Font.center
                                    , Font.size 64
                                    , Font.extraBold
                                    ]
                    in
                    { title = title
                    , children = titleElement :: rest
                    }
                )
                Mark.Custom.title
                ([ typography, widgets, examples, special ]
                    |> List.concat
                    |> Mark.manyOf
                )

        typography =
            [ Mark.Custom.header
            , Mark.Custom.paragraph
            , Mark.Custom.link
            , Mark.Custom.monospace
            , Mark.Custom.emphasize
            , Mark.Custom.subtitle
            , Mark.Custom.list
            , Mark.Custom.image
            ]

        widgets =
            [ Mark.Custom.editor
            , Mark.Custom.elmRepl
            , Mark.Custom.note
            , Mark.Custom.coupon
            ]

        special =
            [ Mark.Custom.window <|
                Mark.oneOf (typography ++ examples ++ widgets)
            , Mark.Custom.row <|
                Mark.manyOf (typography ++ examples ++ widgets)
            ]

        examples =
            [ counter
            , shapes
            , circle
            , protractor
            , gradient
            , nestedTransformations
            , cartesianCoordinates
            , polarCoordinates
            , rosette
            , spiral
            , tree
            , animatedTree
            , viewBox
            ]

        -- Embeded programs' blocks
        protractor : Mark.Block (Examples.Model -> Element Msg)
        protractor =
            let
                render : Examples.Protractor.Config -> Examples.Model -> Element Msg
                render config model =
                    Examples.Protractor.ui config
                        |> Element.el
                            [ Element.centerX
                            , Element.centerY
                            ]
                        |> Element.map Examples.CounterMsg
                        |> Element.map ExamplesMsg
            in
            Mark.record2 "Protractor"
                Examples.Protractor.Config
                (Mark.field "radius" Mark.float)
                (Mark.field "strokeWidth" Mark.float)
                |> Mark.map render

        counter : Mark.Block (Examples.Model -> Element Msg)
        counter =
            let
                render model =
                    model.counter
                        |> Examples.Counter.ui
                        |> Element.el
                            [ Element.centerX
                            , Element.centerY
                            ]
                        |> Element.map Examples.CounterMsg
                        |> Element.map ExamplesMsg
            in
            Mark.stub "Counter" render

        shapes : Mark.Block (Examples.Model -> Element Msg)
        shapes =
            let
                render :
                    Examples.Shapes.Config
                    -> Examples.Model
                    -> Element Msg
                render config model =
                    Examples.Shapes.ui config

                container : Mark.Block Examples.Shapes.Container
                container =
                    Mark.record3 "Container"
                        Examples.Shapes.Container
                        (Mark.field "background" Mark.string)
                        (Mark.field "viewBox" Mark.string)
                        (Mark.field "fill" Mark.bool)

                dot : Mark.Block Examples.Shapes.Shape
                dot =
                    Mark.record3 "Dot"
                        (\radius color transformations ->
                            Examples.Shapes.Dot
                                { transformations = transformations
                                , radius = radius
                                , color = color
                                }
                        )
                        (Mark.field "radius" Mark.float)
                        (Mark.field "color" Mark.string)
                        (Mark.field "transformations"
                            (Mark.manyOf
                                [ identity
                                , translate
                                , rotate
                                ]
                            )
                        )

                line : Mark.Block Examples.Shapes.Shape
                line =
                    Mark.record3 "Line"
                        (\color length transformations ->
                            Examples.Shapes.Line
                                { transformations = transformations
                                , color = color
                                , length = length
                                }
                        )
                        (Mark.field "color" Mark.string)
                        (Mark.field "length" Mark.float)
                        (Mark.field "transformations"
                            (Mark.manyOf
                                [ identity
                                , translate
                                , rotate
                                ]
                            )
                        )

                -- TODO: Implement a recursive group block (that can contain nested groups)
                group : Mark.Block Examples.Shapes.Shape
                group =
                    Mark.record2 "Group"
                        (\children transformations ->
                            Examples.Shapes.Group
                                { transformations = transformations
                                }
                                children
                        )
                        (Mark.field "children" (Mark.manyOf [ dot, line ]))
                        (Mark.field "transformations"
                            (Mark.manyOf
                                [ identity
                                , translate
                                , rotate
                                ]
                            )
                        )

                identity : Mark.Block Transformation
                identity =
                    Mark.stub "Identity" Transformations.Identity

                translate : Mark.Block Transformation
                translate =
                    Mark.record2 "Translate"
                        Transformations.Translate
                        (Mark.field "x" Mark.float)
                        (Mark.field "y" Mark.float)

                rotate : Mark.Block Transformation
                rotate =
                    Mark.block "Rotate" Transformations.Rotate Mark.float
            in
            Mark.block "Shapes"
                render
                (Mark.startWith
                    Examples.Shapes.Config
                    container
                    (Mark.manyOf [ dot, line, group ])
                )

        circle : Mark.Block (Examples.Model -> Element Msg)
        circle =
            let
                render : Examples.Circle.Config -> Examples.Model -> Element Msg
                render config model =
                    Examples.Circle.ui config
            in
            Mark.record7 "Circle"
                Examples.Circle.Config
                (Mark.field "dots" Mark.int)
                (Mark.field "circle" Mark.string)
                (Mark.field "center" Mark.string)
                (Mark.field "radius" Mark.float)
                (Mark.field "radi" Mark.string)
                (Mark.field "protractor" Mark.bool)
                (Mark.field "scatter" Mark.bool)
                |> Mark.map render

        gradient : Mark.Block (Examples.Model -> Element Msg)
        gradient =
            let
                render model =
                    Examples.Gradient.ui
                        |> Element.el
                            [ Element.height (Element.px 400)
                            , Element.width Element.fill
                            ]
            in
            Mark.stub "Gradient" render

        nestedTransformations : Mark.Block (Examples.Model -> Element Msg)
        nestedTransformations =
            let
                render model =
                    model.nestedTransformations
                        |> Examples.NestedTransformations.ui
                        |> Element.el
                            [ Element.width Element.fill
                            ]
                        |> Element.map Examples.NestedTransformationsMsg
                        |> Element.map ExamplesMsg
            in
            Mark.stub "NestedTransformations" render

        cartesianCoordinates : Mark.Block (Examples.Model -> Element Msg)
        cartesianCoordinates =
            let
                render :
                    Examples.CartesianCoordinates.Config
                    -> Examples.Model
                    -> Element Msg
                render config model =
                    model.cartesianCoordinates
                        |> Examples.CartesianCoordinates.ui config
                        |> Element.map Examples.CartesianCoordinatesMsg
                        |> Element.map ExamplesMsg
            in
            Mark.record4 "CartesianCoordinates"
                Examples.CartesianCoordinates.Config
                (Mark.field "minX" Mark.float)
                (Mark.field "minY" Mark.float)
                (Mark.field "maxX" Mark.float)
                (Mark.field "maxY" Mark.float)
                |> Mark.map render

        polarCoordinates : Mark.Block (Examples.Model -> Element Msg)
        polarCoordinates =
            let
                render model =
                    model.polarCoordinates
                        |> Examples.PolarCoordinates.ui
                        |> Element.el
                            [ Element.width Element.fill
                            ]
                        |> Element.map Examples.PolarCoordinatesMsg
                        |> Element.map ExamplesMsg
            in
            Mark.stub "PolarCoordinates" render

        rosette : Mark.Block (Examples.Model -> Element Msg)
        rosette =
            let
                render model =
                    Examples.RosetteTypedTransformations.ui
                        |> Element.el
                            [ Element.height (Element.px 400)
                            , Element.width Element.fill
                            ]
            in
            Mark.stub "Rosette" render

        spiral : Mark.Block (Examples.Model -> Element Msg)
        spiral =
            let
                render model =
                    Examples.Spiral.ui
                        |> Element.el
                            [ Element.height (Element.px 400)
                            , Element.width Element.fill
                            ]
            in
            Mark.stub "Spiral" render

        tree : Mark.Block (Examples.Model -> Element Msg)
        tree =
            let
                render : Examples.Tree.Config -> Examples.Model -> Element Msg
                render config model =
                    Examples.Tree.ui config

                axiom : Mark.Block Examples.Tree.Axiom
                axiom =
                    Mark.record4 "Axiom"
                        Examples.Tree.Axiom
                        (Mark.field "color" Mark.string)
                        (Mark.field "rotation" Mark.float)
                        (Mark.field "age" Mark.float)
                        (Mark.field "growing" Mark.bool)

                rules : Mark.Block (List Examples.Tree.Rule)
                rules =
                    Mark.manyOf [ rule ]

                rule =
                    Mark.record2 "Rule"
                        Tuple.pair
                        (Mark.field "parent" Mark.string)
                        (Mark.field "children" (Mark.manyOf [ child ]))

                parent =
                    Mark.block "Parent" identity Mark.string

                -- Mark.stub "Parent" "green"
                child =
                    Mark.record2 "Child"
                        Examples.Tree.Segment
                        (Mark.field "color" Mark.string)
                        (Mark.field "rotation" Mark.float)
            in
            Mark.block "Tree"
                render
                (Mark.startWith (Examples.Tree.Config "-400 -400 800 800")
                    axiom
                    rules
                )

        animatedTree : Mark.Block (Examples.Model -> Element Msg)
        animatedTree =
            let
                render : Examples.Tree.Config -> Examples.Model -> Element Msg
                render config model =
                    model.animatedTree
                        |> Examples.AnimatedTree.ui config
                        |> Element.map Examples.AnimatedTreeMsg
                        |> Element.map ExamplesMsg

                axiom : Mark.Block Examples.Tree.Axiom
                axiom =
                    Mark.record4 "Axiom"
                        Examples.Tree.Axiom
                        (Mark.field "color" Mark.string)
                        (Mark.field "rotation" Mark.float)
                        (Mark.field "age" Mark.float)
                        (Mark.field "growing" Mark.bool)

                rules : Mark.Block (List Examples.Tree.Rule)
                rules =
                    Mark.manyOf [ rule ]

                rule =
                    Mark.record2 "Rule"
                        Tuple.pair
                        (Mark.field "parent" Mark.string)
                        (Mark.field "children" (Mark.manyOf [ child ]))

                parent =
                    Mark.block "Parent" identity Mark.string

                -- Mark.stub "Parent" "green"
                child =
                    Mark.record2 "Child"
                        Examples.Tree.Segment
                        (Mark.field "color" Mark.string)
                        (Mark.field "rotation" Mark.float)
            in
            Mark.block "AnimatedTree"
                render
                (Mark.startWith (Examples.Tree.Config "-200 -300 400 400")
                    axiom
                    rules
                )

        viewBox : Mark.Block (Examples.Model -> Element Msg)
        viewBox =
            let
                render model =
                    model.viewBox
                        |> Examples.ViewBox.ui
                        |> Element.el
                            [ Element.width Element.fill
                            ]
                        |> Element.map Examples.ViewBoxMsg
                        |> Element.map ExamplesMsg
            in
            Mark.stub "ViewBox" render
    in
    Mark.document
        page
        structure
