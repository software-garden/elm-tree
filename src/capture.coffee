puppeteer = require "puppeteer"
express = require "express"
FS = require "fs"
glob = require "glob"
Path = require "path"

port = 8010

match = (pattern, options) =>
  new Promise (resolve, reject) =>
    glob pattern, options, (error, paths) =>
      if error then return reject errro
      resolve paths

do () =>
  app = express ``
  app.use express.static "public/"

  app.get '*', (req, res) ->
    res.sendFile "container.html", root: process.cwd ``

  server = app.listen port

  browser = await puppeteer.launch
    args: [
      "--no-sandbox"
      "--disable-setuid-sandbox"
    ]

  page = await browser.newPage ``

  page.on "pageerror", (error) ->
    console.error error
    process.exit 1

  # page.on "response", (response) ->
  #   console.log "#{response.status ``} #{response.url ``}"

  page.on "console", (msg) ->
    console.log "# #{page.url ``}: #{msg.text ``}"


  for match in await match "*.txt", cwd: "content/"
    await do (match) ->
      markup = await new Promise (resolve, reject) ->
        FS.readFile "content/" + match, encoding: "utf-8", (error, content) ->
          if error then return reject error

          resolve content

      { dir, name } = Path.parse match
      base = if dir is "" then name else "#{dir}/#{name}"

      url = "http://localhost:#{port}/#{base}.html"
      path = "built/captured/#{base}.html"

      # console.log ""
      # console.log "<-- Catpuring #{url}"
      await page.goto url, waitUntil: "networkidle2"


      html =
        (await page.content ``)
          .replace /<p\b/gi, "<div"
          .replace /<\/p\b/gi, "</div"
          .replace "</body>", """
              <script src="/built/index.js"></script>
              <script>
                var flags = { markup : #{ JSON.stringify markup } }
                Elm.Main.init({ flags: flags })
              </script>
            </body>
          """

      # console.log ""
      # console.log html
      # console.log ""

      # console.log "--> Writing to #{path}"

      await new Promise (resolve, reject) ->
        FS.writeFile path, html, (error) ->
          if error then return reject error
          do resolve

  await browser.close ``
  server.close ``
